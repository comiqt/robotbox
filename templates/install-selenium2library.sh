#!/bin/bash

pip3 freeze |grep robotframework-selenium2library
if [ $? == 0 ]; then
  echo "Already installed"
  exit -1
else
  CURDIR = $(pwd)
  OUT="$(mktemp -d)"
  cd $OUT
  wget https://github.com/HelioGuilherme66/robotframework-selenium2library/archive/v1.8.0b1.tar.gz
  tar xvfz v1.8.0b1.tar.gz
  cd robotframework-selenium2library-1.8.0b1/
  python3 setup.py install
  cd $CURDIR
  rm -rf $OUT
  exit 1
fi
